﻿<%@ Page Language="C#" Inherits="assign1.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
	<form id="form1" runat="server">
        <div>
            <h1>Babysitting Service</h1>
            <br/>
            Parent's name:
	        <asp:TextBox id="parentName" Text="Enter here" runat="server" />
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a name." ControlToValidate="parentName" id="validatorName"></asp:RequiredFieldValidator>
            <br/>
            <br/>
            Parent's phone number:
            <asp:TextBox id="clientphone" Text="Enter here" runat="server" />
            <asp:CompareValidator runat="server" ControlToValidate="clientphone" Type="String" Operator="NotEqual" ValueToCompare="9051234567" ErrorMessage="Please enter a valid phone number." ></asp:CompareValidator>
            <br/>
            <br/>
            Number of kids for babysitting (max. 4):
            <asp:DropDownList runat="server" id="numberofkids">
                <asp:ListItem Value="one" Text="1"></asp:ListItem>
                <asp:ListItem Value="two" Text="2"></asp:ListItem>
                <asp:ListItem Value="three" Text="3"></asp:ListItem>
                <asp:ListItem Value="four" Text="4"></asp:ListItem>
                </asp:DropDownList>
            <br/>
            <br/>
            Kid's age:
            <div id="kidsAge" runat="server">
                <asp:CheckBox runat="server" id="ageRange1" Text="2-3 years old"/>
                <asp:CheckBox runat="server" id="ageRange2" Text="4-6 years old"/>
                <asp:CheckBox runat="server" id="ageRange3" Text="7-10 years old"/>
                <asp:CheckBox runat="server" id="ageRange4" Text="10-13 years old"/>
                </div>
            <br/>
            Parent's home address:
            <asp:TextBox id="clientaddress" Text="Enter here" runat="server" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientaddress" ErrorMessage="Please enter an address."></asp:RequiredFieldValidator>
            <br/>
            <div>
            <asp:RadioButton runat="server" Text="Drop Off" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Pick Up" GroupName="via"/>
            </div>
            <br/>
            Time for service:
            <div id="serviceTime" runat="server">
                <asp:CheckBox runat="server" id="morningService" Text="8am-12pm"/>
                <asp:CheckBox runat="server" id="afternoonService" Text="1pm-5pm"/>
                <asp:CheckBox runat="server" id="eveningService" Text="6pm-11pm"/>
                </div>
            <br/>
            One meal included, choice of meal:
            <div>
            <asp:RadioButton runat="server" Text="Chicken" GroupName="meal"/>
            <asp:RadioButton runat="server" Text="Beef" GroupName="meal"/>
            <asp:RadioButton runat="server" Text="Fish" GroupName="meal"/>
            <asp:RadioButton runat="server" Text="Vegetarian" GroupName="meal"/>
            </div>
            <br/>
            Choice of snack:
            <asp:DropDownList runat="server" id="snack">
                <asp:ListItem Value="cheese" Text="Cheese"></asp:ListItem>
                <asp:ListItem Value="fruit" Text="Fruit"></asp:ListItem>
                <asp:ListItem Value="chocolate" Text="Chocolate"></asp:ListItem>
                <asp:ListItem Value="cookies" Text="Cookies"></asp:ListItem>
                </asp:DropDownList>
            <br/>
            <br/>
            Speical request (optional):
            <br/>
            <asp:TextBox id="specialRequest" TextMode="multiline" Text="e.g. Allergies"runat="server"/>
            <br/>
            <br/>
            <asp:Button runat="server" id="submitform" Text="Submit"/>
            <br/>
            <div runat="server" id="res"></div>
         </div>
	</form>
</body>
</html>
